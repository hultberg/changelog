[![pipeline status](https://gitlab.com/eigan/changelog/badges/master/pipeline.svg)](https://gitlab.com/eigan/changelog/commits/master)
[![coverage report](https://gitlab.com/eigan/changelog/badges/master/coverage.svg)](https://gitlab.com/eigan/changelog/commits/master)


# Changelog
Helps you write changelog

Inspired by how [GitLab handles changelogs](https://docs.gitlab.com/ce/development/changelog.html).



## Usage

#### Write a changelog entry
```
vendor/bin/changelog entry
```

#### Append entries to your CHANGELOG.md
```
vendor/bin/changelog release <version>
```


## TODO
- Documentation
- Better configuration
- Better handling of inserting into CHANGELOG.md
